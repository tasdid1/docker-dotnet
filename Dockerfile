# Stage 1: Build. Use the official .NET Core SDK image as a build environment
FROM mcr.microsoft.com/dotnet/sdk:2.1 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
# COPY hello-world-api/*.csproj ./
# RUN dotnet restore

# Copy the rest of the application code
COPY hello-world-api/. ./
RUN dotnet restore && dotnet publish -c Release -o out
# Build the application
# RUN dotnet publish -c Release -o out

# Stage 2: Run. Use the official ASP.NET Core runtime as the runtime environment
FROM mcr.microsoft.com/dotnet/aspnet:2.1
WORKDIR /app
COPY --from=build-env /app/out .

# Set the environment variable to configure the URL binding
ENV ASPNETCORE_URLS=http://+:80

# Expose the port on which the app will be running
EXPOSE 80

# Set the entry point for the application
ENTRYPOINT ["dotnet", "hello-world-api.dll"]
